package nz.net.pseudo.cb.beacon;


/**
 * Created by i on 25/04/16.
 * Interface between main activity and asynctask
 */
public interface BeaconIface {
    void setBeaconEnabled(boolean enable);
}
